package ru.vmaksimenkov.tm.api;

import ru.vmaksimenkov.tm.dto.AbstractBusinessEntityDTO;

public interface IService<E extends AbstractBusinessEntityDTO> extends IRepository<E> {

}
